import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled, { keyframes } from "styled-components"
import Image from "gatsby-image"
import { graphql } from "gatsby"
import AOS from "aos"
import "aos/dist/aos.css"
import { Element, scroller } from "react-scroll"

import "react-image-gallery/styles/scss/image-gallery.scss"

import ImageGallery from "react-image-gallery"
import "react-image-gallery/styles/css/image-gallery.css"
import "../components/slider.css"
import BackgroundImage from "gatsby-background-image"
import HamburgerMenu from "react-hamburger-menu"
import g1 from "../images/1a.png"
import g2 from "../images/2a.png"
import g3 from "../images/3a.png"
import g4 from "../images/4a.png"
import g5 from "../images/5a.png"
import g6 from "../images/a.png"

export const query = graphql`
  query {
    img1: file(relativePath: { eq: "k.png" }) {
      childImageSharp {
        fixed(width: 512, height: 512) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    successIcon: file(relativePath: { eq: "success.png" }) {
      childImageSharp {
        fixed(width: 32, height: 32) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    img2: file(relativePath: { eq: "instagram.png" }) {
      childImageSharp {
        fixed(width: 32, height: 32) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    mailIcon: file(relativePath: { eq: "mail.png" }) {
      childImageSharp {
        fixed(width: 32, height: 32) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    img3: file(relativePath: { eq: "facebook.png" }) {
      childImageSharp {
        fixed(width: 32, height: 32) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    safety: file(relativePath: { eq: "safety.png" }) {
      childImageSharp {
        fixed(width: 80, height: 80) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    multilang: file(relativePath: { eq: "internet.png" }) {
      childImageSharp {
        fixed(width: 80, height: 80) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    intuitive: file(relativePath: { eq: "intuitive.png" }) {
      childImageSharp {
        fixed(width: 80, height: 80) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    innovation: file(relativePath: { eq: "innovation.png" }) {
      childImageSharp {
        fixed(width: 80, height: 80) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    function: file(relativePath: { eq: "function.png" }) {
      childImageSharp {
        fixed(width: 80, height: 80) {
          ...GatsbyImageSharpFixed
        }
      }
    }

    tmp: file(relativePath: { eq: "mainphoto.png" }) {
      childImageSharp {
        fluid(maxHeight: 540) {
          ...GatsbyImageSharpFluid
        }
      }
    }

    bg2: file(relativePath: { eq: "25060.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1920, maxHeight: 1080) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    photo: file(relativePath: { eq: "1135.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1920, maxHeight: 1080) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    img6: file(relativePath: { eq: "aa.png" }) {
      childImageSharp {
        fixed(width: 400, height: 400) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    img7: file(relativePath: { eq: "contact.png" }) {
      childImageSharp {
        fixed(width: 100, height: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    img8: file(relativePath: { eq: "insta.png" }) {
      childImageSharp {
        fixed(width: 100, height: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    img9: file(relativePath: { eq: "fb.png" }) {
      childImageSharp {
        fixed(width: 100, height: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    secImg: file(relativePath: { eq: "aza.png" }) {
      childImageSharp {
        fluid(maxHeight: 540) {
          ...GatsbyImageSharpFluid
        }
      }
    }

    img11: file(relativePath: { eq: "qr.png" }) {
      childImageSharp {
        fixed(height: 150) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    logo: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        fixed(height: 120) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    logoMobile: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        fixed(height: 80) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`

const MyImage = styled(Image)`
  width: 100%;
`

const btnTransform = keyframes`
  0% {
    
    transform: scale(0.9)
  }

  50% {
    
    transform:  scale(1);
  }
  100% {
    transform: scale(0.9)
  }
`

const ScrollToElement = elementName => {
  scroller.scrollTo(elementName, {
    duration: 1500,
    delay: 100,
    smooth: true,
    offset: 0,
  })
}

const StyledButton = styled.a`
  animation: ${btnTransform} 3s infinite;
  color: white;
  font-weight: 500;
  padding: 10px;
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 30px;
  font-size: 16px;
  cursor: pointer;
  width: 150px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  background: #f857a6; /* fallback for old browsers */
  background: -webkit-linear-gradient(
    to right,
    #ff5858,
    #f857a6
  ); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(
    to right,
    #ff5858,
    #f857a6
  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  z-index: 10;
`
const DescriptionText = styled.div`
  max-width: 600px;
  font-weight: 200;

  @media (max-width: 450px) {
    display: none;
  }
`
const ContactSocialRow = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`

const BgContainer = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-flow: column nowrap;
  padding: 80px;
  @media (max-width: 560px) {
    padding: 40px;
  }
  justify-content: space-between;

  opacity: 1 !important;
  background: rgb(243, 244, 245);
`
const GalleryContainer = styled.div`
  background: rgb(243, 244, 245);
  padding: 50px;
`
const SecondPageContainer = styled(BackgroundImage)`
  padding: 80px;
  @media (max-width: 560px) {
    padding: 20px;
  }
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-flow: column wrap;
  justify-content: center;
  align-items: flex-start;
  opacity: 1 !important;

  background-size: cover;
  background-attachment: fixed;

  background-repeat: no-repeat;
  position: relative;
`

const CardsContainer = styled.div`
  z-index: 10;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`

const Card = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: center;
  width: 300px;
  min-height: 450px;
  border-radius: 10px;
  background: #f857a6; /* fallback for old browsers */
  background: -webkit-linear-gradient(
    to right,
    #ff5858,
    #f857a6
  ); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(
    to right,
    #ff5858,
    #f857a6
  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  color: white;
  padding: 40px;
  margin: 20px;
  box-shadow: 0 0 10px rgba(95, 125, 149, 0.3);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  &:hover {
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  }
`
const CardHeader = styled.div`
  font-size: 30px;
  @media (max-width: 420px) {
    font-size: 20px;
  }
`

const StyledHeader = styled.header`
  display: flex;
  flex-flow: row wrap;
  font-weight: 600;
  width: 100%;
  z-index: 10;
`
const HeaderItem = styled.span`
  margin-right: 15px;
  font-weight: 300;
  cursor: pointer;
  &:hover {
    color: #f857a6;
  }
  @media (max-width: 455px) {
    display: none;
  }
`
const FooterContactBox = styled.div`
  display: flex;
  flex-flow: column wrap;
  align-items: flex-start;
`

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  @media (max-width: 800px) {
    flex-flow: column nowrap;
    flex-direction: column-reverse;
  }
`

const Item = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  @media (max-width: 455px) {
    align-items: center;
  }
`

const ImageItem = styled(Item)`
  height: 100%;

  width: 100%;
  align-items: center;
  z-index: 10;
`

const Description = styled.div`
  display: flex;
  font-weight: 400;
  margin: 20px 0;
  font-size: 20px;
  z-index: 10;

  @media (max-width: 450px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`

const SocialMediaDiv = styled.div`
  display: flex;
  flex-flow: row nowrap;
  z-index: 10;
`

const CardText = styled.div`
  width: 100%;
  font-size: 15px;
  font-weight: 400;
  display: flex;
  justify-content: center;
`

const SecondPageHeader = styled.div`
  z-index: 10;
  width: 100%;
  font-weight: 700;
  font-size: 45px;
  margin: 40px 0;

  @media (max-width: 500px) {
    font-size: 25px;
  }
  display: flex;
  justify-content: center;
  align-items: center;
`

const StyledFooter = styled.footer`
  padding: 80px;
  @media (max-width: 560px) {
    padding: 60px;
  }
  width: 100%;
  background: #23272a;

  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  font-weight: 300;
  font-size: 16px;
  flex-flow: row wrap;
`

const MainPageImageItem = styled(ImageItem)`
  @media (max-width: 1000px) {
    display: none !important;
  }
`

const OpacityDiv = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.7);
`
const MenuContainer = styled.div`
  position: relative;
`

const StyledHamburgerMenu = styled(HamburgerMenu)`
  @media (min-width: 456px) {
    display: none;
  }
`

const Popover = styled.div`
  background: #fff;
  border-radius: 4px;
  width: 200px;
  margin: 20px auto;
  padding: 15px;
  position: absolute;
  display: flex;
  flex-flow: column nowrap;

  box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
`
const IndexPage = ({ data }) => {
  React.useEffect(() => {
    AOS.init()
  }, [])

  const sources1 = [
    {
      ...data.logo.childImageSharp.fixed,
      media: `(min-width: 649px)`,
    },
    {
      ...data.logoMobile.childImageSharp.fixed,
      media: `(max-width: 650px)`,
    },
  ]

  const [isMenuOpen, setIsMenuOpen] = React.useState(false)
  const handleMenuBtnClick = () => {
    setIsMenuOpen(isMenuOpen => !isMenuOpen)
  }

  const PopoverMenuScrollToElement = elementName => {
    scroller.scrollTo(elementName, {
      duration: 1500,
      delay: 100,
      smooth: true,
      offset: 0,
    })
    setIsMenuOpen(false)
  }
  const images = [
    {
      original: `${g1}`,
      thumbnail: `${g1}`,
    },
    {
      original: `${g2}`,
      thumbnail: `${g2}`,
    },
    {
      original: `${g3}`,
      thumbnail: `${g3}`,
    },
    {
      original: `${g4}`,
      thumbnail: `${g4}`,
    },
    {
      original: `${g5}`,
      thumbnail: `${g5}`,
    },
    {
      original: `${g6}`,
      thumbnail: `${g6}`,
    },
  ]

  return (
    <Layout>
      <SEO title="Home" />
      <BgContainer>
        <StyledHeader>
          <MenuContainer>
            <StyledHamburgerMenu
              isOpen={isMenuOpen}
              menuClicked={handleMenuBtnClick}
              width={25}
              height={15}
              strokeWidth={4}
              rotate={0}
              color="black"
              borderRadius={0}
              animationDuration={0.5}
            />
            {isMenuOpen && (
              <Popover>
                <a
                  onClick={() => PopoverMenuScrollToElement("Zalety")}
                  href="/#"
                >
                  Dlaczego warto
                </a>{" "}
                <a
                  onClick={() => PopoverMenuScrollToElement("Realizacje")}
                  href="/#"
                >
                  Realizacje
                </a>
                <a
                  onClick={() => PopoverMenuScrollToElement("Kontakt")}
                  href="/#"
                >
                  Kontakt
                </a>
              </Popover>
            )}
          </MenuContainer>

          <>
            <HeaderItem onClick={() => ScrollToElement("Zalety")}>
              Dlaczego warto
            </HeaderItem>
            <HeaderItem onClick={() => ScrollToElement("Realizacje")}>
              Realizacje
            </HeaderItem>
            <HeaderItem onClick={() => ScrollToElement("Kontakt")}>
              Kontakt
            </HeaderItem>
          </>
        </StyledHeader>
        <Wrapper>
          <Item style={{ margin: "40px 0" }}>
            <MyImage fixed={sources1} style={{ marginBottom: "10px" }} />
            <Description>
              <span
                data-aos="fade-right"
                data-aos-duration="1000"
                data-aos-delay="200"
              >
                Projektujemy.&nbsp;
              </span>
              <span
                data-aos="fade-up"
                data-aos-duration="1000"
                data-aos-delay="500"
              >
                Tworzymy.&nbsp;
              </span>
              <span
                data-aos="fade-left"
                data-aos-duration="1000"
                data-aos-delay="1000"
              >
                Wdrażamy.
              </span>
            </Description>
            <DescriptionText
              data-aos="fade-in"
              data-aos-duration="1000"
              data-aos-delay="2000"
            >
              Tworzymy spersonalizowane emenu dla twojego biznesu. Każdy projekt
              personalizujemy pod wymagania klienta. Zapraszamy do kontaktu w
              celu umówienia się na prezentację!
            </DescriptionText>
            <StyledButton
              onClick={() => ScrollToElement("Zalety")}
              data-aos="fade-right"
              data-aos-duration="2000"
              data-aos-delay="2500"
            >
              Rozpocznij
            </StyledButton>
          </Item>
          <MainPageImageItem>
            <MyImage fluid={data.tmp.childImageSharp.fluid} />
          </MainPageImageItem>
        </Wrapper>
        <SocialMediaDiv>
          <Image
            fixed={data.img2.childImageSharp.fixed}
            style={{ marginRight: "10px" }}
          />
          <Image fixed={data.img3.childImageSharp.fixed} />
        </SocialMediaDiv>
      </BgContainer>
      <Element name="Zalety" />
      <SecondPageContainer
        Tag="div"
        backgroundColor={`#040e18`}
        fluid={data.bg2.childImageSharp.fluid}
      >
        <OpacityDiv />

        <SecondPageHeader>Dlaczego warto?</SecondPageHeader>
        <CardsContainer>
          <Card data-aos="fade-right" data-aos-duration="1000">
            <CardHeader>Bezpieczne</CardHeader>
            <Image
              fixed={data.safety.childImageSharp.fixed}
              style={{ margin: "40px 0" }}
            />
            <CardText>
              Bezdotykowe menu pozwala zachować najwyższą higienę oraz
              bezpieczeństwo.
            </CardText>
          </Card>
          <Card data-aos="fade-right" data-aos-duration="1500">
            <CardHeader>Wielojezyczne</CardHeader>
            <Image
              fixed={data.multilang.childImageSharp.fixed}
              style={{ margin: "40px 0" }}
            />
            <CardText>
              Dzięki nam twoje menu będzie dostępne w wybranych przez Ciebie
              językach.
            </CardText>
          </Card>
          <Card data-aos="fade-right" data-aos-duration="2000">
            <CardHeader>Intuicyjne</CardHeader>
            <Image
              fixed={data.intuitive.childImageSharp.fixed}
              style={{ margin: "40px 0" }}
            />
            <CardText>
              Dostarczane przez nas emenu zawierają intuicyjną nawigację, dzięki
              której każdy klient się odnajdzie.
            </CardText>
          </Card>
          <Card data-aos="fade-right" data-aos-duration="2500">
            <CardHeader>Funkcjonalne</CardHeader>
            <Image
              fixed={data.function.childImageSharp.fixed}
              style={{ margin: "40px 0" }}
            />
            <CardText>
              Jesteśmy w stanie umieścić więcej treści w emenu niż w zwykłym
              papierkowym. Mamy koszyk który pozwoli klientowi na przechowywanie
              potencjalnego zamówienia
            </CardText>
          </Card>
          <Card data-aos="fade-right" data-aos-duration="3000">
            <CardHeader>Innowacyjne</CardHeader>
            <Image
              fixed={data.innovation.childImageSharp.fixed}
              style={{ margin: "40px 0" }}
            />
            <CardText>Przekonaj się sam :)</CardText>
          </Card>
        </CardsContainer>
      </SecondPageContainer>
      <Element name="Realizacje" />
      <GalleryContainer>
        <ImageGallery items={images} />
      </GalleryContainer>
      <StyledFooter>
        <Element name="Kontakt" />
        <FooterContactBox>
          <span
            style={{
              alignSelf: "center",
              color: "pink",
              fontSize: "20px",
              marginBottom: "10px",
            }}
          >
            Kontakt
          </span>
          <ContactSocialRow>
            <Image
              style={{ marginRight: "10px" }}
              fixed={data.mailIcon.childImageSharp.fixed}
            />{" "}
            mateusz@qrest.pl
          </ContactSocialRow>
          <ContactSocialRow>
            <Image
              style={{ marginRight: "10px" }}
              fixed={data.img2.childImageSharp.fixed}
            />{" "}
            Qrest
          </ContactSocialRow>
          <ContactSocialRow>
            <Image
              style={{ marginRight: "10px" }}
              fixed={data.img3.childImageSharp.fixed}
            />{" "}
            Qrest
          </ContactSocialRow>
        </FooterContactBox>
      </StyledFooter>
    </Layout>
  )
}

export default IndexPage
